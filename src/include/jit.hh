#pragma once

#include <asmjit/asmjit.h>

using namespace asmjit;

namespace Wonton::JIT {
   typedef double (*GenericNumberFN)(void);
   typedef void (*GenericNoneFN)(void);

   struct JitInfo {
      JitRuntime& rt;
      x86::Compiler& cc;
      CodeHolder& ch;
   };
}