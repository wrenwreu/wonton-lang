#pragma once

#include <map>
#include <string>
#include <vector>
#include "lexer.hh"
#include "jit.hh"

namespace Wonton {
   union StackValue {
      double dVal;
      const char* sVal;
   };

   struct Stack {
      StackValue stack[1024];
      size_t size = 0;
      size_t idx = -1;

      void push(double);
      void push(const char*);
      void push(StackValue*);
      StackValue* pop();
   };

   class Runtime {
   public:
      Runtime(JitRuntime*, Stack*);
      void init();
      void run(std::vector<Token>*);
   private:
      JitRuntime* rt;
      Stack* stack;
      std::map<std::string, std::vector<Token>> functionTable;

      void push(double);
      void push(const char*);
      double popD();
      const char* popS();
   };
}