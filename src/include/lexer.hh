#pragma once

#include <vector>
#include <string>

namespace Wonton {
   enum class TokenKind {
      ATOM,
      NUMBER,
      STRING,
      PLUS,
      DASH,
      STAR,
      FSLASH,
      GT, LT,
      DOT,
      COMMA,
      COLON,
      SEMICOLON,
      IF,
      ELSE,
      END,
      DUP,
      SWAP,
      EOI
   };

   struct Token {
      TokenKind kind;
      double dVal;
      std::string sVal;
      int line;
   };

   std::vector<Token> tokenize(std::string&);
}