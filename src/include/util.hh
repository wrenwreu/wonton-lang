#pragma once

#include <string>

namespace Wonton {
   std::string readFile(std::string);
}