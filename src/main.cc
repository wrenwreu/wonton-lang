#include <iostream>
#include "include/jit.hh"
#include "include/util.hh"
#include "include/lexer.hh"
#include "include/runtime.hh"

int main(int argc, const char** argv) {
   if (argc < 2) {
      std::cerr << "Expected an argument." << std::endl;
      return 1;
   }

   auto file = Wonton::readFile(argv[1]);

   auto tokens = Wonton::tokenize(file);

   JitRuntime rt;
   Wonton::Stack s;

   Wonton::Runtime wtnrt(&rt, &s);
   wtnrt.init();
   wtnrt.run(&tokens);

   return 0;
}