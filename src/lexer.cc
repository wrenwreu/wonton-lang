#include <iostream>
#include "include/lexer.hh"

Wonton::Token resolveToken(std::string content, int line) {
   using namespace Wonton;
   Token t;
   // Welcome to the mega-chain!
   // C++ doesn't have string switching!
   if (content == "+") {
      t.kind = TokenKind::PLUS;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == "-") {
      t.kind = TokenKind::DASH;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == "*") {
      t.kind = TokenKind::STAR;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == "/") {
      t.kind = TokenKind::FSLASH;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == ">") {
      t.kind = TokenKind::GT;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == "<") {
      t.kind = TokenKind::LT;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == ".") {
      t.kind = TokenKind::DOT;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == ",") {
      t.kind = TokenKind::COMMA;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == ":") {
      t.kind = TokenKind::COLON;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == ";") {
      t.kind = TokenKind::SEMICOLON;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == "if") {
      t.kind = TokenKind::IF;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == "else") {
      t.kind = TokenKind::ELSE;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == "end") {
      t.kind = TokenKind::END;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == "dup") {
      t.kind = TokenKind::DUP;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else if (content == "swap") {
      t.kind = TokenKind::SWAP;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   else {
      t.kind = TokenKind::ATOM;
      t.dVal = 0;
      t.sVal = content;
      t.line = line;
   }
   return t;
}

std::vector<Wonton::Token> Wonton::tokenize(std::string& file) {
   using namespace Wonton;
   std::vector<Token> tmp;
   size_t idx = 0;
   int line = 1;
   std::string tmpStr = "";

   while (idx < file.length()) {
      if (idx >= file.length()) {
         break;
      }
      if (std::isdigit(file.at(idx))) {
         while (std::isdigit(file.at(idx)) || file.at(idx) == '.') {
            tmpStr += file.at(idx);
            idx++;
            if (idx >= file.length()) {
               break;
            }
         }
         Token t;
         t.kind = TokenKind::NUMBER;
         t.dVal = std::stod(tmpStr);
         t.sVal = tmpStr;
         t.line = line;
         tmp.push_back(t);
      }
      else if (std::isspace(file.at(idx))) {
         while (std::isspace(file.at(idx))) {
            if (file.at(idx) == '\n') line++;
            idx++;
            if (idx >= file.length()) {
               break;
            }
         }
      }
      else if (file.at(idx) == '"') {
         idx++;
         while (file.at(idx) != '"') {
            tmpStr += file.at(idx);
            idx++;
         }
         idx++;
         Token t;
         t.kind = TokenKind::STRING;
         t.dVal = 0;
         t.sVal = tmpStr;
         t.line = line;
         tmp.push_back(t);
      }
      else {
         while (!std::isspace(file.at(idx))) {
            tmpStr += file.at(idx);
            idx++;
            if (idx == file.length()) break;
         }
         Token t = resolveToken(tmpStr, line);
         tmp.push_back(t);
      }
      tmpStr = "";
   }
   Token eof;
   eof.kind = TokenKind::EOI;
   eof.dVal = 3.1415;
   eof.sVal = "EOI";
   eof.line = line;
   tmp.push_back(eof);

   return tmp;
} 