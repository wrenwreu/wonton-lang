#include <cstdlib>
#include <iostream>
#include "include/runtime.hh"

using namespace Wonton;

void Stack::push(double d) {
   StackValue sv;
   sv.dVal = d;
   stack[size] = sv;
   size++;
   idx++;
}

void Stack::push(const char* s) {
   StackValue sv;
   sv.sVal = s;
   stack[size] = sv;
   size++;
   idx++;
}

void Stack::push(StackValue* sv) {
   stack[size] = *sv;
   size++;
   idx++;
}

StackValue* Stack::pop() {
   size--;
   return &stack[idx--];
}

Runtime::Runtime(JitRuntime* r, Stack* s) : rt{ r }, stack{ s } {}

void* funcTable[256];

typedef double (*TMP)(double a, double b);

void Runtime::init() {
   CodeHolder ch;
   ch.init(rt->environment(), rt->cpuFeatures());
   x86::Compiler cc(&ch);
   FuncNode* fnp = cc.addFunc(FuncSignatureT<double, double, double>());
   x86::Xmm vReg0 = cc.newXmm();
   x86::Xmm vReg1 = cc.newXmm();
   Label l0 = cc.newLabel();
   Label l1 = cc.newLabel();
   fnp->setArg(0, vReg0);
   fnp->setArg(1, vReg1);
   cc.comisd(vReg1, vReg0);
   cc.jbe(l0);
   cc.jmp(l1);
   cc.bind(l0);
   cc.pxor(vReg0, vReg0);
   cc.bind(l1);
   cc.ret(vReg0);
   cc.endFunc();
   cc.finalize();
   TMP lt;
   rt->add(&lt, &ch);
   ch.reset();
   funcTable[0] = (void*)lt;

   CodeHolder ch1;
   ch1.init(rt->environment(), rt->cpuFeatures());
   x86::Compiler cc1(&ch1);
   FuncNode* fnp1 = cc1.addFunc(FuncSignatureT<double, double, double>());
   x86::Xmm vReg0_1 = cc1.newXmm();
   x86::Xmm vReg1_1 = cc1.newXmm();
   Label l0_1 = cc1.newLabel();
   Label l1_1 = cc1.newLabel();
   fnp1->setArg(0, vReg0_1);
   fnp1->setArg(1, vReg1_1);
   cc1.comisd(vReg0_1, vReg1_1);
   cc1.jbe(l0_1);
   cc1.jmp(l1_1);
   cc1.bind(l0_1);
   cc1.pxor(vReg0_1, vReg0_1);
   cc1.bind(l1_1);
   cc1.ret(vReg0_1);
   cc1.endFunc();
   cc1.finalize();
   TMP gt;
   rt->add(&gt, &ch1);
   ch1.reset();
   funcTable[1] = (void*)gt;

   CodeHolder ch2;
   ch2.init(rt->environment(), rt->cpuFeatures());
   x86::Compiler cc2(&ch2);
   FuncNode* fnp2 = cc2.addFunc(FuncSignatureT<double, double, double>());
   x86::Xmm vReg0_2 = cc2.newXmm();
   x86::Xmm vReg1_2 = cc2.newXmm();
   fnp2->setArg(0, vReg0_2);
   fnp2->setArg(1, vReg1_2);
   cc2.divsd(vReg0_2, vReg1_2);
   cc2.ret(vReg0_2);
   cc2.endFunc();
   cc2.finalize();
   TMP div;
   rt->add(&div, &ch2);
   ch2.reset();
   funcTable[2] = (void*)div;

   CodeHolder ch3;
   ch3.init(rt->environment(), rt->cpuFeatures());
   x86::Compiler cc3(&ch3);
   FuncNode* fnp3 = cc3.addFunc(FuncSignatureT<double, double, double>());
   x86::Xmm vReg0_3 = cc3.newXmm();
   x86::Xmm vReg1_3 = cc3.newXmm();
   fnp3->setArg(0, vReg0_3);
   fnp3->setArg(1, vReg1_3);
   cc3.mulsd(vReg0_3, vReg1_3);
   cc3.ret(vReg0_3);
   cc3.endFunc();
   cc3.finalize();
   TMP mul;
   rt->add(&mul, &ch3);
   ch3.reset();
   funcTable[3] = (void*)mul;

   CodeHolder ch4;
   ch4.init(rt->environment(), rt->cpuFeatures());
   x86::Compiler cc4(&ch4);
   FuncNode* fnp4 = cc4.addFunc(FuncSignatureT<double, double, double>());
   x86::Xmm vReg0_4 = cc4.newXmm();
   x86::Xmm vReg1_4 = cc4.newXmm();
   fnp4->setArg(0, vReg0_4);
   fnp4->setArg(1, vReg1_4);
   cc4.subsd(vReg0_4, vReg1_4);
   cc4.ret(vReg0_4);
   cc4.endFunc();
   cc4.finalize();
   TMP sub;
   rt->add(&sub, &ch4);
   ch4.reset();
   funcTable[4] = (void*)sub;

   CodeHolder ch5;
   ch5.init(rt->environment(), rt->cpuFeatures());
   x86::Compiler cc5(&ch5);
   FuncNode* fnp5 = cc5.addFunc(FuncSignatureT<double, double, double>());
   x86::Xmm vReg0_5 = cc5.newXmm();
   x86::Xmm vReg1_5 = cc5.newXmm();
   fnp5->setArg(0, vReg0_5);
   fnp5->setArg(1, vReg1_5);
   cc5.addsd(vReg0_5, vReg1_5);
   cc5.ret(vReg0_5);
   cc5.endFunc();
   cc5.finalize();
   TMP add;
   rt->add(&add, &ch5);
   ch5.reset();
   funcTable[5] = (void*)add;
}

void Runtime::run(std::vector<Token>* toks) {
   double ifRes;
   size_t idx = 0;
   while (idx < toks->size()) {
      switch (toks->at(idx).kind) {
         case TokenKind::ATOM: {
            if (functionTable.contains(toks->at(idx).sVal)) {
               run(&functionTable[toks->at(idx).sVal]);
            }
            else {
               push(toks->at(idx).sVal.c_str());
            }
            break;
         }
         case TokenKind::NUMBER: {
            push(toks->at(idx).dVal);
            break;
         }
         case TokenKind::STRING: {
            push(toks->at(idx).sVal.c_str());
            break;
         }
         case TokenKind::PLUS: {
            TMP tmp = (TMP)funcTable[5];
            double b = popD();
            double a = popD();
            push(tmp(a, b));
            break;
         }
         case TokenKind::DASH: {
            TMP tmp = (TMP)funcTable[4];
            double b = popD();
            double a = popD();
            push(tmp(a, b));
            break;
         }
         case TokenKind::STAR: {
            TMP tmp = (TMP)funcTable[3];
            double b = popD();
            double a = popD();
            push(tmp(a, b));
            break;
         }
         case TokenKind::FSLASH: {
            TMP tmp = (TMP)funcTable[2];
            double b = popD();
            double a = popD();
            push(tmp(a, b));
            break;
         }
         case TokenKind::GT: {
            TMP tmp = (TMP)funcTable[1];
            double b = popD();
            double a = popD();
            push(tmp(a, b));
            break;
         }
         case TokenKind::LT: {
            TMP tmp = (TMP)funcTable[0];
            double b = popD();
            double a = popD();
            push(tmp(a, b));
            break;
         }
         case TokenKind::DOT:
            printf("%g\n", popD());
            break;
         case TokenKind::COMMA:
            printf("%s\n", popS());
            break;
         case TokenKind::COLON: {
            std::vector<Token> tmp;
            idx++;
            while (toks->at(idx).kind != TokenKind::SEMICOLON) {
               tmp.push_back(toks->at(idx));
               idx++;
            }
            functionTable[std::string(popS())] = tmp;
            break;
         }
         case TokenKind::SEMICOLON: {
            break;
         }
         case TokenKind::IF: {
            ifRes = popD();
            if (ifRes == 0) {
               while (toks->at(idx).kind != TokenKind::ELSE && toks->at(idx).kind != TokenKind::END) {
                  idx++;
               }
            }
            break;
         }
         case TokenKind::ELSE: {
            ifRes = popD();
            if (ifRes != 0) {
               while (toks->at(idx).kind != TokenKind::END && toks->at(idx).kind != TokenKind::EOI) {
                  idx++;
               }
            }
            break;
         }
         case TokenKind::END: {
            break;
         }
         case TokenKind::DUP: {
            StackValue sv = *stack->pop();
            stack->push(&sv);
            stack->push(&sv);
            break;
         }
         case TokenKind::SWAP: {
            StackValue b = *stack->pop();
            StackValue a = *stack->pop();
            stack->push(&b);
            stack->push(&a);
            break;
         }
         case TokenKind::EOI: {
            return;
         }
      }
      idx++;
   }
}

void Runtime::push(double d) {
   stack->push(d);
}

void Runtime::push(const char* s) {
   stack->push(s);
}

double Runtime::popD() {
   return stack->pop()->dVal;
}

const char* Runtime::popS() {
   return stack->pop()->sVal;
}