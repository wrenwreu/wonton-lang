#include <fstream>
#include "include/util.hh"

std::string Wonton::readFile(std::string path) {
   std::ifstream ifs(path);
   std::string file(
      (std::istreambuf_iterator<char>(ifs)),
      (std::istreambuf_iterator<char>())
   );
   ifs.close();
   return file;
}