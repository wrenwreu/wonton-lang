# Wonton

Wonton is a FORTH-like stack oriented language interpreter that utilizes an experimental JIT runtime. So far only some operations are executed using the JIT runtime and the current goal is to have as much executed by the JIT engine as possible.

Code in this repository is an extreme case of spaghetti but you can try to read through it if you feel like it.

***Currently the JIT runtime only supports x86_64!!!***

## Requirements

- An x86_64 CPU
- A C++ compiler that supports **C++20**
- GNU Make
- [AsmJit](https://asmjit.com/)

## Good old hello world

```
"Hello, world!" ,
```