exec = wonton
CXX = clang++
SRC = $(wildcard src/*.cc)
OBJ = $(SRC:.cc=.o)
CXXFLAGS = -std=c++20 -Wall -O3 -march=native
LDFLAGS = -lasmjit -lstdc++ -flto -lm

$(exec): $(OBJ)
	$(CC) $(OBJ) $(CXXFLAGS) -o $(exec) $(LDFLAGS)

%.o: %.cc include/%.hh
	$(CC) -c $(CXXFLAGS) $< -o $@ $(LDFLAGS)

clean:
	clear
	rm src/*.o