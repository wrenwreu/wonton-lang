# Words

| Name   | Description                                                                    |
|--------|--------------------------------------------------------------------------------|
| `+`    | Floating point addition.                                                       |
| `-`    | Floating point subtraction.                                                    |
| `*`    | Floating point multiplication.                                                 |
| `/`    | Floating point division.                                                       |
| `>`    | Greater than comparison. Pushes a nonzero value when the condition holds true. |
| `<`    | Less than comparison. Pushes a nonzero value when the condition holds true.    |
| `.`    | Prints a floating point value to the standard output.                          |
| `,`    | Prints a string value to the standard output.                                  |
| `:`    | Begins a new word.                                                             |
| `;`    | Ends a word.                                                                   |
| `if`   | Begins a conditional if statement.                                             |
| `else` | Begins the else section of an if statement.                                    |
| `end`  | Ends a statement.                                                              |
| `dup`  | Duplicates the top of the stack.                                               |
| `swap` | Swaps the top two values on the stack.                                         |